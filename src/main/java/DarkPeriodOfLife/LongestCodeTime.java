package DarkPeriodOfLife;

import java.io.*;

/**
 * Created by HaChi on 12/6/2014.
 */
public class LongestCodeTime {


    public static void main(String[] args) throws IOException {

        File path = new File("DayOfRoy.txt");
        BufferedReader  reader = new BufferedReader(new InputStreamReader(
                new FileInputStream(path)));
        BufferedReader  reader2 = new BufferedReader(new InputStreamReader(
                new FileInputStream(path)));
        String line = new String() ;
        int x = getLongestCodeTimeOfDay(reader,line);
        int m = getLongestCodeTimeOfPeriod(reader2, line);
        reader.close() ;
        reader2.close() ;
        System.out.println(x);
        System.out.println(m);

    }
   /*******khoang thoi gian code lien tuc dai nhat*******/
    private static int getLongestCodeTimeOfPeriod(BufferedReader reader, String line) throws IOException {
        int longestStreakOfPeriod = 0;
        int StreakOfDay = 0 ;
        int longestStreak = 0;
        line = reader.readLine();
        while (line != null) {
            for (int j = 0; j < 1440; j++) {
                char result = line.charAt(j);
                if (result != 'C') {
                    longestStreakOfPeriod = max(longestStreakOfPeriod, StreakOfDay);
                    StreakOfDay = 0;
                }
                else {
                    StreakOfDay += 1;
                }
            }
         //   System.out.println(" 1 "+line);
            line = reader.readLine();
        }
        return longestStreakOfPeriod;
    }
    /*******khoang thoi gian code dai nhat trong ngay*******/
   private static int getLongestCodeTimeOfDay(BufferedReader reader, String line) throws IOException {
        int longestStreakEachDay = 0;
        int StreakOfDayCode = 0 ;
        int longestStreakOfDays = 0;
       line = reader.readLine();
        while (line != null) {
            for (int j = 0; j < 1440; j++) {
                char result = line.charAt(j);
                if ((result != 'C') || (j == 1339)) {
                    longestStreakEachDay = max(longestStreakEachDay, StreakOfDayCode);
                    StreakOfDayCode = 0;
                }
                else {
                    StreakOfDayCode += 1;
                }
                longestStreakOfDays = max(longestStreakOfDays, longestStreakEachDay);
            }
          //  System.out.println(line);
                    line = reader.readLine();
        }
        return longestStreakOfDays;
    }


    private static int max(int longestStreakOfDayCode, int streakOfCode) {
        return longestStreakOfDayCode = longestStreakOfDayCode > streakOfCode ? longestStreakOfDayCode : streakOfCode;
    }
}